package envdir

import "testing"

func TestIncorrectCmd(t *testing.T) {
	cmd := []string{"env", "-incorrect"}
	env := map[string]string{
		"SERVER_KEY": "qwas1234",
	}
	code := RunCmd(cmd, env)
	if code == 0 {
		t.Fatal("Exit Code Error")
	}
}

func TestCmd(t *testing.T) {
	cmd := []string{"env"}
	env := map[string]string{
		"SERVER_KEY": "qwas1234",
	}
	code := RunCmd(cmd, env)
	if code != 0 {
		t.Fatal("Exit Code Error")
	}
}
