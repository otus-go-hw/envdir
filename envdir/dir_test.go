package envdir

import (
	"io/ioutil"
	"testing"
)

func createFile(name string, data string) error {
	fileBytes := []byte(data)
	err := ioutil.WriteFile(name, fileBytes, 0644)
	return err
}

func TestReadEmptyEnv(t *testing.T) {
	dir := "./env/"
	envClientName := "CLIENT_KEY"
	err := createFile(dir+envClientName, "")
	if err != nil {
		t.Fatal("error on created env file")
	}
	err = createFile(dir+envClientName, "")

	env, err := ReadDir(dir)
	if err != nil {
		t.Fatal("error on created env file")
	}

	if env[envClientName] != "" {
		t.Fatalf("bad result, expected %v=", envClientName)
	}
}

func TestRead(t *testing.T) {
	dir := "./env/"
	envServerName := "SERVER_KEY"
	envServerValue := "qwas1234"
	err := createFile(dir+envServerName, envServerValue)
	if err != nil {
		t.Fatal("error on created env file")
	}
	envCountryName := "COUNTRY"
	envCountryValue := "RUSSIA"
	err = createFile(dir+envCountryName, envCountryValue)
	if err != nil {
		t.Fatal("error on created env file")
	}

	env, err := ReadDir(dir)
	if err != nil {
		t.Fatal("error on read env file")
	}

	if env[envServerName] == "" {
		t.Fatalf("bad result, expected %v=%v", envServerName, envServerValue)
	}
	if env[envCountryName] == "" {
		t.Fatalf("bad result, expected %v=%v", envCountryName, envCountryName)
	}
}
