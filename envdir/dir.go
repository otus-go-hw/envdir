package envdir

import (
	"io/ioutil"
	"path/filepath"
)

func ReadDir(dirname string) (map[string]string, error) {
	env := make(map[string]string)
	files, err := ioutil.ReadDir(dirname)
	if err != nil {
		return env, err
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}
		path := filepath.Join(dirname, file.Name())
		bytes, err := ioutil.ReadFile(path)
		if err != nil {
			return env, err
		}
		env[file.Name()] = string(bytes)
	}
	return env, nil
}