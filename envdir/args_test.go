package envdir

import "testing"

func TestArgsIsEmpty(t *testing.T) {
	args := []string{"", ""}
	_, _, err := RunArgs(args)
	if err == nil {
		t.Fatalf("TestArgsIsEmpty: There must be an error")
	}
}

func TestDirArg(t *testing.T) {
	args := []string{"/etc", "cmd"}
	_, _, err := RunArgs(args)
	if err != nil {
		t.Fatalf("dir should not have passed")
	}
}


func TestSliceArgsIsEmpty(t *testing.T) {
	var args []string
	_, _, err := RunArgs(args)
	if err == nil {
		t.Fatalf("TestSliceArgsIsEmpty: If the arguments are empty there should be an error")
	}
}

func TestFirstArgIsEmpty(t *testing.T) {
	args := []string{""}
	_, _, err := RunArgs(args)
	if err == nil {
		t.Fatalf("TestFirstArgIsEmpty: If the arguments are empty there should be an error")
	}
}
