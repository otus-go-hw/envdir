package envdir

import (
	"errors"
	"os"
	"strings"
)

func RunArgs(args []string) (dir string, cmd []string, err error) {
	if len(args) < 2 {
		err = errors.New("wrong arguments")
		return "", nil, err
	}

	dir = args[0]
	if len(strings.TrimSpace(dir)) == 0 {
		err = errors.New("argument dir is empty")
		return "", nil, err
	}
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = errors.New("directory isn`t exists")
		return "", nil, err
	}

	cmd = args[1:]
	if len(strings.TrimSpace(cmd[0])) == 0 {
		err = errors.New("argument cmd is empty")
		return dir, nil, err
	}

	return dir, cmd, nil
}
