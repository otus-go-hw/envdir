package envdir

import (
	"os"
	"os/exec"
)

func RunCmd(cmd []string, env map[string]string) int {
	name := cmd[0]
	args := cmd[1:]

	var envArr []string
	for key, value := range env {
		envArr = append(envArr, key+"="+value)
	}

	proc := exec.Command(name, args...)
	proc.Stdout = os.Stdout
	proc.Stdin = os.Stdin
	proc.Stderr = os.Stderr
	proc.Env = append(os.Environ(), envArr...)
	if err := proc.Run(); err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			return exitError.ExitCode()
		}
	}

	return 0
}
