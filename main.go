package main

import (
	"gitlab.com/otus-go-hw/envdir/envdir"
	"log"
	"os"
)

func main() {
	args := os.Args[1:]
	dir, cmd, err := envdir.RunArgs(args)
	if err != nil {
		log.Fatal(err)
	}

	env, err := envdir.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	os.Exit(envdir.RunCmd(cmd, env))
}
